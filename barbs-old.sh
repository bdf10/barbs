#!/bin/sh
# Brian's Auto Rice Boostrapping Script (BARBS)
# by Brian Fitzpatrick <bfitzpat@math.duke.edu>
# License: GNU GPLv3

### OPTIONS AND VARIABLES ###

while getopts ":a:r:b:p:h" o;
do case "${o}" in
       h) printf "Optional arguments for custom use:\\n  -r: Dotfiles repository (local file or url)\\n  -p: Dependencies and programs csv (local file or url)\\n  -a: AUR helper (must have pacman-like syntax)\\n  -h: Show this message\\n" && exit 1 ;;
       r) dotfilesrepo=${OPTARG} && git ls-remote "$dotfilesrepo" || exit 1 ;;
       b) repobranch=${OPTARG} ;;
       p) progsfile=${OPTARG} ;;
       a) aurhelper=${OPTARG} ;;
       *) printf "Invalid option: -%s\\n" "$OPTARG" && exit 1 ;;
   esac
done

[ -z "$dotfilesrepo" ] && dotfilesrepo="https://github.com/lukesmithxyz/voidrice.git"
[ -z "$progsfile" ] && progsfile="https://gitlab.oit.duke.edu/bdf10/barbs/-/raw/master/progs.csv"
[ -z "$aurhelper" ] && aurhelper="paru"
[ -z "$repobranch" ] && repobranch="master"

### FUNCTIONS ###

installpkg() { pacman --noconfirm --needed -S "$1" >/dev/null 2>&1 ;}

error() { clear; printf "ERROR:\\n%s\\n" "$1" >&2; exit 1;}

prettypac() { # Make pacman colorful and add eye candy on the progress bar.
    grep -q "^Color" /etc/pacman.conf || sed -i "s/^#Color$/Color/" /etc/pacman.conf
    grep -q "ILoveCandy" /etc/pacman.conf || sed -i "/#VerbosePkgLists/a ILoveCandy" /etc/pacman.conf ;}

selectdsk() { disk=$(dialog --stdout --menu "Select the disk for $bios partitioning." 0 0 0 $(lsblk -dplnx size -o name,size | grep -Ev "boot|rpmb|loop" | tac)) ;}

partitiondsk() { # Partition $disk.
    dialog --yesno "Do you want to partition $disk according to $bios?" 0 0 ||
	error "User decided not to partition $disk."
    dialog --yesno "Do you want to continue? This will wipe all the data on $disk!" 0 0 ||
	error "User chickened out of partitioning $disk."
    dialog --infobox "Partitioning $disk according to $bios." 0 0
    case $bios in
	"efi") sfdisk -q --delete $disk &&
		     echo "label: dos" | sfdisk -q $disk &&
		     echo ';' | sfdisk -q $disk ;;
	"uefi") sgdisk -Z $disk >/dev/null 2>&1 &&
		    sgdisk -n 0:0:+450M -t 0:ef00 -c 0:"EFI system partition" $disk >/dev/null 2>&1 &&
		    sgdisk -n 0:0:0 -t 0:8300 -c 0:"Linux filesystem" $disk >/dev/null 2>&1 ;;
	*) error "bios type $bios unrecognized" ;;
    esac
    partprobe ;}

dskpart() { lsblk -nlpo NAME,TYPE -x SIZE $disk | awk '/part$/ {print $1}' | sed "$1q;d" ;}

fmtparts() { # Format the partition(s) on $disk.
    case $bios in
	"efi") yes | mkfs.ext4 -q $(dskpart 1) ;;
	"uefi") yes | mkfs.fat -F32 $(dskpart 1) >/dev/null 2>&1 &&
		    yes | mkfs.ext4 -q $(dskpart 2) ;;
	*) error "bios type $bios unrecognized" ;;
    esac ;}

mntparts() { # Mount the partitions on $disk.
    case $bios in
	"efi") mount $(dskpart 1) /mnt ;;
	"uefi") mount $(dskpart 2) /mnt &&
		      mkdir -p /mnt/boot &&
		      mount $(dskpart 1) /mnt/boot ;;
	*) error "bios type $bios unrecognized" ;;
    esac ;}

settime() { ln -sf /usr/share/zoneinfo/America/New_York /etc/localtime && hwclock --systohc ;}

setlocale() { sed -i 's/^#en_US.UTF-8/en_US.UTF-8/g' /etc/locale.gen && locale-gen >/dev/null 2>&1 && echo "LANG=en_US.UTF-8" >> /etc/locale.conf ;}

sethost() { # Set the host name.
    host=$(dialog --stdout --inputbox "Enter the name of the machine." 10 60) || exit 1
    while ! echo "$host" | grep -q "^[a-z_][a-z0-9_-]*$"; do
	host=$(dialog --stdout --no-cancel --inputbox "Host name not valid. Enter a host name beginning with a letter, with only lowercase letters, - or _." 10 60)
    done
    echo $host > /etc/host
    printf "127.0.0.1 localhost\n::1       localhost\n127.0.1.1 $host.localdomain $host" > /etc/hosts ;}

rootpass() { # Set the root password.
    pass1=$(dialog --stdout --no-cancel --passwordbox "Enter a root password." 10 60)
    pass2=$(dialog --stdout --no-cancel --passwordbox "Retype root password." 10 60)
    while ! [ "$pass1" = "$pass2" ]; do
	unset pass2
	pass1=$(dialog --stdout --no-cancel --passwordbox "Passwords do not match.\\n\\nEnter root password again." 10 60)
	pass2=$(dialog --stdout --no-cancel --passwordbox "Retype root password." 10 60)
    done
    echo "root:$pass1" | chpasswd
    unset pass1 pass2 ;}

mkgrub() { # Install GRUB according to $bios.
    pacman --noconfirm --needed -S grub intel-ucode
    case $bios in
	"efi") grub-install --target=i386-pc $(lsblk -npo pkname $(findmnt -no SOURCE /)) ;;
	"uefi") pacman --noconfirm --needed -S efibootmgr &&
		      grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB ;;
	*) error "bios type $bios unrecognized" ;;
    esac
    grub-mkconfig -o /boot/grub/grub.cfg ;}

setusr() { # Set the username as $name.
    name=$(dialog --stdout --inputbox "Enter the username." 10 60) || exit 1
    while ! echo "$name" | grep -q "^[a-z_][a-z0-9_-]*$"; do
	name=$(dialog --stdout --no-cancel --inputbox "Invalid username. Try again." 10 60)
    done
    dialog --infobox "Adding user \"$name\"..." 4 50
    pacman --noconfirm --needed -S zsh opendoas
    useradd -m -g wheel -s /bin/zsh "$name"
    printf "permit persist $name as root\npermit nopass root as $name" > /etc/doas.conf
    doas -u $name mkdir -p /home/$name/.local/src ;}

usrpass() { # Set the password for $name.
    pass1=$(dialog --stdout --no-cancel --passwordbox "Enter the password for $name." 10 60)
    pass2=$(dialog --stdout --no-cancel --passwordbox "Confirm the password for $name." 10 60)
    while ! [ "$pass1" = "$pass2" ]; do
	unset pass2
	pass1=$(dialog --stdout --no-cancel --passwordbox "Passwords do not match.\\n\\nEnter password again." 10 60)
	pass2=$(dialog --stdout --no-cancel --passwordbox "Retype password." 10 60)
    done
    echo "$name:$pass1" | chpasswd
    unset pass1 pass2 ;}

mkaur() { # Installs $aurhelper manually. Used only for AUR helper here.
    [ "$aurhelper" = "paru" ] && pacman --noconfirm --needed -S rust
    cd /tmp/ &&
	curl -O https://aur.archlinux.org/cgit/aur.git/snapshot/"$aurhelper".tar.gz &&
	doas -u "$name" tar -xvf "$aurhelper".tar.gz &&
	cd "$aurhelper" &&
	doas -u "$name" makepkg --noconfirm -si ;}

installationloop() { # Install all the programs.
    ([ -f "$progsfile" ] && cp "$progsfile" /tmp/progs.csv) || curl -Ls "$progsfile" | sed '/^#/d' > /tmp/progs.csv
    total=$(wc -l < /tmp/progs.csv)
    aurinstalled=$(pacman -Qqm)
    while IFS=, read -r tag program service;
    do
	n=$((n+1))
	case "$tag" in
	    "A") aurinstall "$program" ;;
	    "G") gitmakeinstall "$program" ;;
	    *) maininstall "$program" ;;
	esac
	[ -z "$service" ] || systemctl enable -q $service
    done < /tmp/progs.csv ;}

### CONSTANTS ###

ls /sys/firmware/efi/efivars >/dev/null 2>&1 && bios="uefi" || bios="efi"

### PRE CHROOT FUNCTION ###

prechroot() { # This runs if / is mounted to "airootfs".
    # install dialog and git
    prettypac &&
	pacman -Sy --needed --noconfirm dialog git ||
	    error "Failed to prettify pacman or install dialog and git."

    # welcome the user
    dialog --title "Welcome!" --msgbox "Welcome to Brian's Auto-Rice Bootstrapping Script!" 0 0 ||
	error "Something went wrong while displaying the welcome message."

    # give the user a chance to bail
    dialog --colors --title "Install arch linux?" --yes-label "Proceed" --no-label "Abort!" --yesno "You are logged into the arch iso and about to install arch linux. Do you want to proceed with the installation?" 0 0 ||
	error "Failed while asking the user to initiate the install."

    # update mirrors
    dialog --infobox "Updating mirrors." 0 0 &&
	reflector --latest 5 --protocol https --country US --sort rate --save /etc/pacman.d/mirrorlist ||
	    error "Failed to update mirrors."

    # updating keyrings
    dialog --infobox "Updating keyrings." 0 0 &&
	pacman -Sy --noconfirm archlinux-keyring ||
	    error "Failed to update keyrings."

    # setting the clock
    dialog --infobox "Setting the clock." 0 0 &&
	timedatectl set-ntp true ||
	    error "Failed to set the clock."

    # set the $disk variable
    selectdsk || error "Failed while setting the \$disk variable."

    # partition $disk according to $bios
    partitiondsk || error "Failed while partitioning $disk."

    # format $disk according to $bios
    dialog --infobox "Formatting the partitions on $disk according to $bios." 0 0 &&
	fmtparts || error "Failed while formatting the partitions on $disk."

    # mount $disk partitions according to $bios
    dialog --infobox "Mounting partitions on $disk according to $bios." 0 0 &&
	mntparts || error "Failed while mounding the partitions on $disk."

    # install the bare essentials
    dialog --infobox "Installing the bare essentials." 0 0 &&
	pacstrap /mnt base base-devel linux linux-firmware dialog git opendoas ||
	    error "Failed while installing the bare essentials."

    # generate /mnt/etc/fstab
    dialog --infobox "Generating /mnt/etc/fstab" 0 0 &&
	genfstab -U /mnt >> /mnt/etc/fstab ||
	    error "Failed while generating fstab."

    # clone barbs into /mnt/root/
    dialog --infobox "Cloning barbs into /mnt/root/barbs" 0 0 &&
	git clone https://gitlab.oit.duke.edu/bdf10/barbs.git /mnt/root/barbs ||
	    error "Failed while cloning BARBS into /mnt/root/."

    # time to chroot!
    dialog --infobox "Chrooting!" 0 0 &&
	arch-chroot /mnt bash /root/barbs/barbs.sh ||
	    error "Failed while attempting to chroot." ;}

### POST CHROOT FUNCTION ###

postchroot() { # This is the main function if running as chroot.
    # prettifying pacman
    dialog --infobox "prettifying pacman." 0 0 &&
	prettypac || error "Failed while prettifying pacman."

    # set time
    dialog --infobox "Setting the time." 0 0 &&
	settime || error "Failed while setting the time."

    # set locale
    dialog --infobox "Generating locale." 0 0 &&
	setlocale || error "Failed while generating the locale."

    # set host
    sethost || error "Failed while setting the host."

    # set root password
    rootpass || error "Failed while setting the root password."

    # install and configure grub
    dialog --infobox "Installing GRUB." 0 0 &&
	mkgrub || error "Failed while installing GRUB."

    # set user
    setusr || error "Failed while setting the user."

    # set user password
    usrpass || error "Failed while setting the password for $name."

    # Use all cores for compilation.
    dialog --infobox "Activating all cores for package compilation." 0 0 &&
	sed -i "s/-j2/-j$(nproc)/;s/^#MAKEFLAGS/MAKEFLAGS/" /etc/makepkg.conf || error "Failed while activating all cores for package compilation"

    # Install AUR helper.
    mkaur $aurhelper || error "Failed while installing AUR helper."

    # The command that does all the installing. Reads the progs.csv file and
    # installs each needed program the way required. Be sure to run this only after
    # the user has been created and has priviledges to run sudo without a password
    # and all build dependencies are installed.
    installationloop

    # set swappiness to 0
    dialog --infobox "Setting swappiness to zero." 0 0 && echo "vm.swappiness=0" > /etc/sysctl.d/99-swappiness.conf || error "Failed while setting swappiness to zero."

    # configure reflector
    # (should change this to do an installation check)
    dialog --infobox "Configuring reflector." 0 0 && printf -- "--save /etc/pacman.d/mirrorlist\n--country US\n--protocol https\n--sort rate\n--latest 10" > /etc/xdg/reflector/reflector.conf || error "Failed while configuring reflector."

    # configure paru to use doas
    dialog --infobox "Configuring paru to use doas." 0 0 && sed -i 's/^#\[bin\]/\[bin\]/g ; s/^#Sudo = doas/Sudo = doas/g ; s/^SudoLoop/#SudoLoop/g' /etc/paru.conf || error "Failed while configuring paru to use doas."

    # done!
    dialog --title "Done!" --msgbox "Arch has been installed!" 0 0 || error "Failed while congratulating user for finishing the install." ;}

### SCRIPT INVOCATION ###

[ "$(findmnt -no SOURCE /)" = "airootfs" ] && prechroot || postchroot
